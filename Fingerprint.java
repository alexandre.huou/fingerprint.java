package cs107;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Provides tools to compare fingerprint.
 */
public class Fingerprint {

  /**
   * The number of pixels to consider in each direction when doing the linear
   * regression to compute the orientation.
   */
  public static final int ORIENTATION_DISTANCE = 16;

  /**
   * The maximum distance between two minutiae to be considered matching.
   */
  public static final int DISTANCE_THRESHOLD = 5;

  /**
   * The number of matching minutiae needed for two fingerprints to be considered
   * identical.
   */
  public static final int FOUND_THRESHOLD = 20;

  /**
   * The distance between two angle to be considered identical.
   */
  public static final int ORIENTATION_THRESHOLD = 20;

  /**
   * The offset in each direction for the rotation to test when doing the
   * matching.
   */
  public static final int MATCH_ANGLE_OFFSET = 2;

  /**
   * Returns an array containing the value of the 8 neighbours of the pixel at
   * coordinates <code>(row, col)</code>.
   * <p>
   * The pixels are returned such that their indices corresponds to the following
   * diagram:<br>
   * ------------- <br>
   * | 7 | 0 | 1 | <br>
   * ------------- <br>
   * | 6 | _ | 2 | <br>
   * ------------- <br>
   * | 5 | 4 | 3 | <br>
   * ------------- <br>
   * <p>
   * If a neighbours is out of bounds of the image, it is considered white.
   * <p>
   * If the <code>row</code> or the <code>col</code> is out of bounds of the
   * image, the returned value should be <code>null</code>.
   *
   * @param image array containing each pixel's boolean value.
   * @param row   the row of the pixel of interest, must be between
   *              <code>0</code>(included) and
   *              <code>image.length</code>(excluded).
   * @param col   the column of the pixel of interest, must be between
   *              <code>0</code>(included) and
   *              <code>image[row].length</code>(excluded).
   * @return An array containing each neighbours' value.
   */
  public static boolean[] getNeighbours(boolean[][] image, int row, int col) {
	  assert (image != null); // special case that is not expected (the image is supposed to have been checked
                              // earlier)
	  
	  boolean[] voisin = new boolean [8];
	
	  	
	  if (ComputeValue(image, row-1, col)) {
		  
		 	voisin[0]=image[row-1][col];
		 	
	  }else voisin[0]=false;
	  
	  
	  if (ComputeValue(image, row-1, col+1)) {
		  
		 	voisin[1]=image[row-1][col+1];
		 	
	  }else voisin[1]=false;
	  
	  
	  if (ComputeValue(image, row, col+1)) {
		  
		 	voisin[2]=image[row][col+1];
		 	
	  }else voisin[2]=false;
	  
	  
	  if (ComputeValue(image, row+1, col+1)) {
		  
		 	voisin[3]=image[row+1][col+1];
		 	
	  }else voisin[3]=false;
	  
	  
	  if (ComputeValue(image, row+1, col)) {
		  
		 	voisin[4]=image[row+1][col];
		 	
	  }else voisin[4]=false;
	  
	  
	  if (ComputeValue(image, row+1, col-1)) {
		  
		 	voisin[5]=image[row+1][col-1];
		 	
	  }else voisin[5]=false;
	  
	  
	  if (ComputeValue(image, row, col-1)) {
		  
		 	voisin[6]=image[row][col-1];
		 	
	  }else voisin[6]=false;
	  
	  
	  if (ComputeValue(image, row-1, col-1)) {
		  
		 	voisin[7]=image[row-1][col-1];
		 	
	  }else voisin[7]=false;
	  
	  
	  return voisin;
  }
  
  
  public static boolean ComputeValue (boolean[][] image, int row, int col) {
	  if (row<0 || col<0 || row+1>image.length || col+1>image[0].length) {
		  
		  return false;
	  }else {
		  return true;
	  }
  }
		  

  /**
   * Computes the number of black (<code>true</code>) pixels among the neighbours
   * of a pixel.
   *
   * @param neighbours array containing each pixel value. The array must respect
   *                   the convention described in
   *                   {@link #getNeighbours(boolean[][], int, int)}.
   * @return the number of black neighbours.
   */
  public static int blackNeighbours(boolean[] neighbours) {
	  //TODO implement
	  
	  int count_Black=0;
	  //Pour chacun des 8 voisins
	  for (int i=0 ; i<neighbours.length ; ++i) {
		//si un voisin est noir, true+1
		  if(neighbours[i]==true) {
			  ++count_Black;
		 
		  }
	  }
	  //Resultat noñbre de voisins noirs
	  return count_Black;
  }
  
  /**
   * Computes the number of white to black transitions among the neighbours of
   * pixel.
   *
   * @param neighbours array containing each pixel value. The array must respect
   *                   the convention described in
   *                   {@link #getNeighbours(boolean[][], int, int)}.
   * @return the number of white to black transitions.
   */
  public static int transitions(boolean[] neighbours) {
	  //Nombre de transition
	  int count_Transitions=0;
	  //Pour chacun des 8 voisins
	  for (int i=1 ; i<neighbours.length ; ++i) {
		  //Cas particulier du retour a la case depart
		  if (i==7 && (neighbours[i]==false) && (neighbours[0]==true)) {
			  ++count_Transitions;
		  }
		  // Quand on passe d´une case blanche a une case noire +1
		  if ((neighbours[i-1]==false) && (neighbours[i]==true)) {
			  ++count_Transitions;
			  
			  
		  }
	  }
	  
	  
	  return count_Transitions;
  }

  /**
   * Returns <code>true</code> if the images are identical and false otherwise.
   *
   * @param image1 array containing each pixel's boolean value.
   * @param image2 array containing each pixel's boolean value.
   * @return <code>True</code> if they are identical, <code>false</code>
   *         otherwise.
   */
  public static boolean identical(boolean[][] image1, boolean[][] image2) {
	  //TODO implement
		  //Pour chqaue ligne
		  for (int i=0, i2=0 ; i<image1.length && i2<image2.length; ++i, ++i2) {
			  //Pour chaque cologne, une fois toutes les colones de row 0 testees, on passe a row+1
			  for(int j=0, j2=0 ; j<image1[i].length && j2<image2[i2].length ; ++j , ++j2) {
			  //Si les iñqges ne sont pqs de la meme taille
				  if((image1[i][j] != image2[i2][j2]) || (image1.length!=image2.length) || (image1[i].length != image2[i2].length)) {
					  		/*Si les 2 images n'ont pas la même dimension alors la fonction en soit 
					  		 * est inutile et on entrera dans une boucle infinie de rerun du code supposé 
					  		 * modifier les deux images... Or on ne peut ni print ni return 
					  		 * une exception car l'algo nous contera cette manifestation extra 
					  		 * comme fausse non? Peut on supposer que les images ont la même taille?
					  		 */
					  		
			  
					  return false;
					  
				  	}
				  
			  }
			  
		}return true;
		
  }
 
  

  /**
   * Internal method used by {@link #thin(boolean[][])}.
   *
   * @param image array containing each pixel's boolean value.
   * @param step  the step to apply, Step 0 or Step 1.
   * @return A new array containing each pixel's value after the step.
   */
  public static boolean[][] thinningStep(boolean[][] image, int step) {
	  //TROUVER LA TAILLE DE NOTRE COPIE DE IMAGE[][]
	  
	  
	  boolean[][] image_Copie = new boolean [image.length][image[0].length];
		

			for (int row1=0 ; row1<image.length ; ++row1) {
	  
				for (int col1=0 ; col1<image[0].length ; ++col1) {
			
					image_Copie [row1][col1] = image[row1][col1];
				  
		   					}
			 
		  				 }
			
		  					
	  //START DE PHASE 1/2
			
			
		
				
	  for (int row = 0 ; row<image.length ; ++row) {
		  
		  for (int col = 0 ; col<image[0].length ; ++col) {
		  
			  
			  // Deux boucles for pour parcourir toute l'image en tant que tableau
			  
			//CAS DE LA PHASE 1
			  
		
		  if (image[row][col] == true) {
			  	//Cas ou pixel est noir
			  
			  if ((getNeighbours(image, row, col)) != null) {
				  	//Cas ou tableau des 8 voisins non nul
				  
				 if ((blackNeighbours((getNeighbours(image, row, col))) >= 2) && ((blackNeighbours(Fingerprint.getNeighbours(image, row, col))) <= 6)) {
					 	//2 <= Voisins noirs <= 6
					 
					 if ((transitions(Fingerprint.getNeighbours(image, row, col))) == 1) {
						 	//Une seule transition de blanc vers noir
						 
						 
						 
						 if (step==0) {
							 
							 if (((getNeighbours(image, row, col)[0])==false || (getNeighbours(image, row, col)[2])==false || (getNeighbours(image, row, col)[4])== false)) {
								 	//P0, P2, P4 blancs

								 if (((getNeighbours(image, row, col)[2])==false || (getNeighbours(image, row, col)[4])==false || (getNeighbours(image, row, col)[6])==false)) {
									 	//P2, P4, P6 blancs 
									 
									 image_Copie [row][col] = false;
								 			//Pixel en question mis en pixel blanc dans copie de l'image
									  
								 }
							 }
							
							 
		
								 }if (step==1) {
									 
									 if (((getNeighbours(image, row, col)[0])==false || (getNeighbours(image, row, col)[2])==false || (getNeighbours(image, row, col)[6]) == false)) {
										 	//P0, P2, P6 blancs

										 if (((getNeighbours(image, row, col)[0])==false || (getNeighbours(image, row, col)[4])==false || (getNeighbours(image, row, col)[6])==false)) {
											 	//P0, P4, P6 blancs 
											 
											 image_Copie [row][col] = false;

										 			//Pixel en question mis en pixel blanc dans copie de l'image
								 
										 			}
										 
									 			}
									 
								 			}
								 
							 }
							 
						 }
						 
					 }
					 
				 }
				 
			  }
			  
		  }
		  
	  if (step == 0) {
	  	
  		return image_Copie;
  										
	}else if (step == 1) {
	  	
		return image_Copie;
	  									
	 }else {
		 
		 System.out.println("Testfail");
	  	return image;
	  		
	 }
	  	
  		}
  										

 
  
  /**
   * Compute the skeleton of a boolean image.
   *
   * @param image array containing each pixel's boolean value.
   * @return array containing the boolean value of each pixel of the image after
   *         applying the thinning algorithm.
   */
  public static boolean[][] thin(boolean[][] image) {

 	  boolean[][] new_Image_Thinned = new boolean [image.length][image[0].length];
	  boolean[][] previous_Image_Thinned = new boolean [image.length][image[0].length];
	 
	  
	  
	  for (int row=0 ; row<image.length ; ++row) {
		  
		  for (int col = 0 ; col<image[0].length ; ++col) {
			  
			  new_Image_Thinned[row][col] = image [row][col];
			  
		  		}
	  		}
	  
			 
	  do {
	
						previous_Image_Thinned = new_Image_Thinned;
								
						new_Image_Thinned = thinningStep(previous_Image_Thinned, 0);
						
						new_Image_Thinned = thinningStep(new_Image_Thinned, 1);
						
			 
	  }while (!identical(new_Image_Thinned, previous_Image_Thinned));
			 
		 return new_Image_Thinned;
		 	
			 }
  
  /**
   * Computes all pixels that are connected to the pixel at coordinate
   * <code>(row, col)</code> and within the given distance of the pixel.
   *
   * @param image    array containing each pixel's boolean value.
   * @param row      the first coordinate of the pixel of interest.
   * @param col      the second coordinate of the pixel of interest.
   * @param distance the maximum distance at which a pixel is considered.
   * @return An array where <code>true</code> means that the pixel is within
   *         <code>distance</code> and connected to the pixel at
   *         <code>(row, col)</code>.
   */
  public static boolean[][] connectedPixels(boolean[][] image, int row, int col, int distance) {
	  //TODO implement
	  
boolean[][] tab_Connected_Pixels_Bef = new boolean[image.length][image[0].length];	
tab_Connected_Pixels_Bef[row][col]=true;
boolean[][] tab_Connected_Pixels_After = new boolean[image.length][image[0].length];	
tab_Connected_Pixels_After[row][col]=true;
int new_Distance_Ligne;
int new_Distance_Col;
boolean connecte = false;

//methode: CONECTE PAR RAPORT A TAB:
	
//Cas particulier ou distance de pt à bord vertical > taille verticale de tab
if (row-distance<0 || row+distance>image.length) {
	new_Distance_Ligne=row;
}else {
	new_Distance_Ligne = distance;
}


//Cas particulier ou distance de pt à bord horizontal > taille horizontale de tab
if (col-distance<0 || col+distance>image[0].length) {
	new_Distance_Col=col;
}else {
	new_Distance_Col = distance;
}

if (new_Distance_Col<new_Distance_Ligne) {
	new_Distance_Col = new_Distance_Ligne;
}else {
	new_Distance_Ligne = new_Distance_Col;
}



 
do {	

	
for (int row1=0 ; row1<image.length ; ++row1) {
	for (int col1=0 ; col1<image[0].length ; ++col1) {
	
tab_Connected_Pixels_Bef[row1][col1] = tab_Connected_Pixels_After[row1][col1];

	}
}

	
for (int row1=0 ; row1<image.length ; ++row1) {
	for (int col1=0 ; col1<image[0].length ; ++col1) {
		
		
		if (image[row1][col1] == true) {
			  
			  
			  if ((row-new_Distance_Ligne<=row1 && row1<=row+new_Distance_Ligne) && (col-new_Distance_Col<=col1 && col1<=col+new_Distance_Col)) {
				  
				  
				  for (int i = 0 ; i<8 ; ++i) {
					  
					  if (getNeighbours(tab_Connected_Pixels_Bef, row1, col1) [i] == true) {
						  
						  connecte = true;
								  
					  }
					  			  
				  }if (connecte == true) {
					  tab_Connected_Pixels_After[row1][col1] = true;
				  }
				  
				  connecte=false;
				  
				  
				  
			  }
			   
		  }
		
		
		
		
	}
}
	  
  
}while (!identical(tab_Connected_Pixels_After, tab_Connected_Pixels_Bef));
	
	
  
return tab_Connected_Pixels_After;

}
  
  /**
   * Computes the slope of a minutia using linear regression.
   *
   * @param connectedPixels the result of
   *                        {@link #connectedPixels(boolean[][], int, int, int)}.
   * @param row             the row of the minutia.
   * @param col             the col of the minutia.
   * @return the slope.
   */
  public static double computeSlope(boolean[][] connectedPixels, int row, int col) {
	  //TODO implement
	  
	  
	//Calcul de x
	 
	  double sum_x2=0;
	  double sum_y2=0;
	  double sum_xy = 0;
	  double a=0;
	  
	  for (int row1=0 ; row1<connectedPixels.length; ++row1) {
		  for (int col1 = 0 ; col1<connectedPixels[0].length ; ++col1) {
			  
			  if (connectedPixels[row1][col1]) {
				  
				  sum_x2 += ((col1-col)*(col1-col));
				  sum_y2 += ((row - row1)*(row - row1));
				  sum_xy += ((col1-col)*(row-row1));
				  
			  }
			  
		  }
	  }
		 
	  
	  if (sum_x2==0) {
		  
		  a = Double.POSITIVE_INFINITY;  
	 
	  }else if (sum_x2 >= sum_y2) {
		  
		  a = (sum_xy)/(sum_x2);
		  
	  }else if (sum_x2<sum_y2) {
		  
		  a = (sum_y2)/(sum_xy);
		  
	  }return a;
	  
  }

  /**
   * Computes the orientation of a minutia in radians.
   * 
   * @param connectedPixels the result of
   *                        {@link #connectedPixels(boolean[][], int, int, int)}.
   * @param row             the row of the minutia.
   * @param col             the col of the minutia.
   * @param slope           the slope as returned by
   *                        {@link #computeSlope(boolean[][], int, int)}.
   * @return the orientation of the minutia in radians.
   */
  public static double computeAngle(boolean[][] connectedPixels, int row, int col, double slope) {

double angle = Math.atan(slope);
int Dessus = 0;
int Dessous = 0;
int x = 0;
int y = 0;

for (int row1 = 0 ; row1<connectedPixels.length ; ++row1) {
	for (int col1 = 0 ; col1<connectedPixels[row1].length ; ++col1) {
		
		y=row-row1;
		x=col1-col;
			
			if (connectedPixels[row1][col1]) {
		
			if ((y)>=(-1/slope)*(x)) {
				
				++Dessus;
				
			}else {
				
				++Dessous;
				
				}
			}
		}
	
	}
	
if (angle == 0) {
	
	angle = Math.abs(angle);
	
	return angle;
	
}else if (slope == Double.POSITIVE_INFINITY) {
	
	if (Dessus>=Dessous) {
		angle = (Math.PI)/2;
	}else {
		angle = -(Math.PI)/2;	
		
	}return angle;

}else if ((angle > 0 && Dessous>Dessus) || (angle<0 && Dessous<Dessus)) {
	
	angle += Math.PI;
	
	return angle;
	
}
	    
	  return angle;
  }
  
  
  
  

  /**
   * Computes the orientation of the minutia that the coordinate <code>(row,
   * col)</code>.
   *
   * @param image    array containing each pixel's boolean value.
   * @param row      the first coordinate of the pixel of interest.
   * @param col      the second coordinate of the pixel of interest.
   * @param distance the distance to be considered in each direction to compute
   *                 the orientation.
   * @return The orientation in degrees.
   */
  public static int computeOrientation(boolean[][] image, int row, int col, int distance) {
	  
boolean[][] tab_Connected_Pix = connectedPixels(image, row, col, distance);
double Compute_Slope = computeSlope(tab_Connected_Pix, row, col);
double Compute_Angle = computeAngle(tab_Connected_Pix, row, col, Compute_Slope);










double Angle_Degre = Math.round(Math.toDegrees(Compute_Angle));



if (Angle_Degre<0) {
	 Angle_Degre += 360;	
}
 
 	  return (int) Angle_Degre;
  }

  /**
   * Extracts the minutiae from a thinned image.
   *
   * @param image array containing each pixel's boolean value.
   * @return The list of all minutiae. A minutia is represented by an array where
   *         the first element is the row, the second is column, and the third is
   *         the angle in degrees.
   * @see #thin(boolean[][])
   */
  public static List<int[]> extract(boolean[][] image) {

boolean[] voisins = getNeighbours(image, 1, 1);	  

List <int[]> minuties = new ArrayList <int[]>();

int[] tab_Minutia = new int [3];
	  
for (int row = 1 ; row<image.length-1 ; ++row) {
	for (int col = 1 ; col<image[0].length-1 ; ++col) {
	
	voisins = getNeighbours(image, row, col);	 
	
	if (image[row][col]==true) {
		
		if ((transitions(voisins) == 1) || (transitions(voisins)==3)){
			
			tab_Minutia[0]=row;
			tab_Minutia[1]=col;
			tab_Minutia[2]=computeOrientation(image, row, col, ORIENTATION_DISTANCE);
			
			minuties.add(tab_Minutia.clone());
			
			}
		
		}
	
	}

}
  
	  return minuties;
  }

  /**
   * Applies the specified rotation to the minutia.
   *
   * @param minutia   the original minutia.
   * @param centerRow the row of the center of rotation.
   * @param centerCol the col of the center of rotation.
   * @param rotation  the rotation in degrees.
   * @return the minutia rotated around the given center.
   */
  public static int[] applyRotation(int[] minutia, int centerRow, int centerCol, int rotation) {
	  //TODO implement
	  return null;
  }

  /**
   * Applies the specified translation to the minutia.
   *
   * @param minutia        the original minutia.
   * @param rowTranslation the translation along the rows.
   * @param colTranslation the translation along the columns.
   * @return the translated minutia.
   */
  public static int[] applyTranslation(int[] minutia, int rowTranslation, int colTranslation) {
	  //TODO implement
	  return null;
  } 
  
  /**
   * Computes the row, column, and angle after applying a transformation
   * (translation and rotation).
   *
   * @param minutia        the original minutia.
   * @param centerCol      the column around which the point is rotated.
   * @param centerRow      the row around which the point is rotated.
   * @param rowTranslation the vertical translation.
   * @param colTranslation the horizontal translation.
   * @param rotation       the rotation.
   * @return the transformed minutia.
   */
  public static int[] applyTransformation(int[] minutia, int centerRow, int centerCol, int rowTranslation,
      int colTranslation, int rotation) {
	  //TODO implement
	  return null;
  }

  /**
   * Computes the row, column, and angle after applying a transformation
   * (translation and rotation) for each minutia in the given list.
   *
   * @param minutiae       the list of minutiae.
   * @param centerCol      the column around which the point is rotated.
   * @param centerRow      the row around which the point is rotated.
   * @param rowTranslation the vertical translation.
   * @param colTranslation the horizontal translation.
   * @param rotation       the rotation.
   * @return the list of transformed minutiae.
   */
  public static List<int[]> applyTransformation(List<int[]> minutiae, int centerRow, int centerCol, int rowTranslation,
      int colTranslation, int rotation) {
	  //TODO implement
	  return null;
  }
  /**
   * Counts the number of overlapping minutiae.
   *
   * @param minutiae1      the first set of minutiae.
   * @param minutiae2      the second set of minutiae.
   * @param maxDistance    the maximum distance between two minutiae to consider
   *                       them as overlapping.
   * @param maxOrientation the maximum difference of orientation between two
   *                       minutiae to consider them as overlapping.
   * @return the number of overlapping minutiae.
   */
  public static int matchingMinutiaeCount(List<int[]> minutiae1, List<int[]> minutiae2, int maxDistance,
      int maxOrientation) {
	  //TODO implement
	  return 0;
  }

  /**
   * Compares the minutiae from two fingerprints.
   *
   * @param minutiae1 the list of minutiae of the first fingerprint.
   * @param minutiae2 the list of minutiae of the second fingerprint.
   * @return Returns <code>true</code> if they match and <code>false</code>
   *         otherwise.
   */
  public static boolean match(List<int[]> minutiae1, List<int[]> minutiae2) {
	  //TODO implement
	  return false;
  }
